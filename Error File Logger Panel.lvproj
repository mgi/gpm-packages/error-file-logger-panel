﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@mgi" Type="Folder">
				<Item Name="detailed-error" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Detailed Error.lvlib" Type="Library" URL="../gpm_packages/@mgi/detailed-error/Source/MGI-Detailed Error.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/detailed-error/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/detailed-error/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/detailed-error/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/detailed-error/README.md"/>
				</Item>
				<Item Name="error-log" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Error Log.lvlib" Type="Library" URL="../gpm_packages/@mgi/error-log/Source/MGI-Error Log.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/error-log/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/error-log/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/error-log/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/error-log/README.md"/>
				</Item>
				<Item Name="error-logger-panel" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Error Logger Panel.lvlib" Type="Library" URL="../gpm_packages/@mgi/error-logger-panel/Source/MGI-Error Logger Panel.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/error-logger-panel/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/error-logger-panel/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/error-logger-panel/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/error-logger-panel/README.md"/>
				</Item>
				<Item Name="error-reporter-core" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Error Reporter Core.lvlib" Type="Library" URL="../gpm_packages/@mgi/error-reporter-core/Source/MGI-Error Reporter Core.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/error-reporter-core/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/error-reporter-core/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/error-reporter-core/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/error-reporter-core/README.md"/>
				</Item>
				<Item Name="library-core" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Library Core.lvlib" Type="Library" URL="../gpm_packages/@mgi/library-core/Source/MGI-Library Core.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/library-core/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/library-core/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/library-core/LICENSE"/>
					<Item Name="readme.md" Type="Document" URL="../gpm_packages/@mgi/library-core/readme.md"/>
				</Item>
				<Item Name="monitored-actor-core" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Monitored Actor Core.lvlib" Type="Library" URL="../gpm_packages/@mgi/monitored-actor-core/Source/MGI-Monitored Actor Core.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/monitored-actor-core/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/monitored-actor-core/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/monitored-actor-core/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/monitored-actor-core/README.md"/>
				</Item>
				<Item Name="panel-actor-error-reporter" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Panel Actor Error Reporter.lvlib" Type="Library" URL="../gpm_packages/@mgi/panel-actor-error-reporter/Source/MGI-Panel Actor Error Reporter.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/panel-actor-error-reporter/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/panel-actor-error-reporter/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/panel-actor-error-reporter/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/panel-actor-error-reporter/README.md"/>
				</Item>
				<Item Name="panel-actors" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Panel Actor.lvlib" Type="Library" URL="../gpm_packages/@mgi/panel-actors/Source/MGI-Panel Actor.lvlib"/>
						<Item Name="Panel Actor Event Loop.vi" Type="VI" URL="../gpm_packages/@mgi/panel-actors/Source/Panel Actor Event Loop.vi"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/panel-actors/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/panel-actors/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/panel-actors/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/panel-actors/README.md"/>
				</Item>
				<Item Name="panel-manager" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Panel Manager.lvlib" Type="Library" URL="../gpm_packages/@mgi/panel-manager/Source/MGI-Panel Manager.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/panel-manager/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/panel-manager/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/panel-manager/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/panel-manager/README.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="MGI-Error File Logger Panel.lvlib" Type="Library" URL="../Source/MGI-Error File Logger Panel.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Number To Enum.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Number To Enum.vim"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="OffsetRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/OffsetRect.vi"/>
				<Item Name="Open a Document on Disk.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open a Document on Disk.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Single String To Qualified Name Array.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Single String To Qualified Name Array.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Maximize.lvclass" Type="LVClass" URL="../gpm_packages/@mgi/panel-actors/gpm_packages/@mgi/panel-manager/Source/Window Position/Maximize/Maximize.lvclass"/>
			<Item Name="Panel Type Selector.vi" Type="VI" URL="../gpm_packages/@mgi/panel-actors/gpm_packages/@mgi/panel-manager/Source/Panel Type Selector.vi"/>
			<Item Name="Panel.lvclass" Type="LVClass" URL="../gpm_packages/@mgi/panel-actors/gpm_packages/@mgi/panel-manager/Source/Panel/Panel.lvclass"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Window Position.lvclass" Type="LVClass" URL="../gpm_packages/@mgi/panel-actors/gpm_packages/@mgi/panel-manager/Source/Window Position/Window Position.lvclass"/>
			<Item Name="Window.lvclass" Type="LVClass" URL="../gpm_packages/@mgi/panel-actors/gpm_packages/@mgi/panel-manager/Source/Panel/Window/Window.lvclass"/>
			<Item Name="Window.vi" Type="VI" URL="../gpm_packages/@mgi/panel-actors/gpm_packages/@mgi/panel-manager/Source/Panel/Window/Window.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
